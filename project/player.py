"""Player class"""

from turtle import Turtle

STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280


class Player(Turtle):
    """Player class"""

    def __init__(self):
        """Init turtle player"""
        super().__init__()
        self.shape("turtle")
        self.setheading(90)
        self.penup()
        self.start()

    def start(self):
        """Go to the start position"""
        self.goto(STARTING_POSITION)

    def move(self):
        """Move the turtle"""
        self.forward(MOVE_DISTANCE)

    def chech_finish_line(self):
        """Check if turtle reached the finish line"""
        if self.ycor() >= FINISH_LINE_Y:
            self.start()
            return True
        return False
