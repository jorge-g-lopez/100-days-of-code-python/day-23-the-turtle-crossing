"""The Turtle Crossing Game"""

import time
from turtle import Screen

from car_manager import CarManager
from player import Player
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=600, height=600)
screen.title("Turtle Crossing")
screen.tracer(0)
screen.listen()

scoreboard = Scoreboard()

player = Player()
cars = CarManager()
screen.onkey(player.move, "Up")

game_is_on = True
loop_counter = 0

while game_is_on:
    time.sleep(0.1)
    cars.move()
    loop_counter += 1

    if player.chech_finish_line():
        scoreboard.level_up()
        cars.move_increment()

    if cars.over_turtle(player):
        game_is_on = False
        scoreboard.game_over()

    if loop_counter == 9 and len(cars.cars) < 30:
        cars.new_car()
        loop_counter = 1

    screen.update()

screen.exitonclick()
