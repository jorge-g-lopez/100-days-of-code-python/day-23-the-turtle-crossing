"""Cars class"""

import random
from turtle import Turtle

COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MOVE_DISTANCE = 5
MOVE_INCREMENT = 5


class CarManager:
    """Cars class"""

    def __init__(self):
        """Init cars"""
        self.move_distance = STARTING_MOVE_DISTANCE
        self.cars = []
        self.new_car()

    def new_car(self):
        """Add a new car"""
        new_car = Turtle("square")
        new_car.penup()
        new_car.shapesize(stretch_len=2, stretch_wid=1)
        new_car.color(random.choice(COLORS))
        new_car.setheading(180)
        new_car.goto(270, random.randint(-250, 250))
        self.cars.append(new_car)

    def move(self):
        """Move cars"""
        for i, car in enumerate(self.cars):
            self.cars[i].forward(self.move_distance)

            if car.xcor() < -320:
                self.cars[i].goto(300, car.ycor())

    def move_increment(self):
        """Increment movement"""
        self.move_distance += MOVE_INCREMENT

    def over_turtle(self, turtle):
        """Check if a car is over the turtle"""

        for i, car in enumerate(self.cars):
            if car.distance(turtle) < 21:
                return True
        return False
