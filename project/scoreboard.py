"""Scoreboard class"""

from turtle import Turtle

FONT = ("Courier", 24, "normal")


class Scoreboard(Turtle):
    """Scoreboard class"""

    def __init__(self):
        """Init scoreboard"""
        super().__init__()
        self.hideturtle()
        self.penup()
        self.level = 1
        self.update_scoreboard()

    def update_scoreboard(self):
        """Update the scoreboard"""
        self.clear()
        self.goto(-250, 260)
        self.write(f"Level: {self.level}", font=FONT)

    def level_up(self):
        """Increase the level counter"""
        self.level += 1
        self.update_scoreboard()

    def game_over(self):
        """Print Game Over legend"""
        self.goto(0, 0)
        self.write("GAME OVER", align="center", font=FONT)
